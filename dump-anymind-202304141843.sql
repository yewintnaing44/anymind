--
-- PostgreSQL database dump
--

-- Dumped from database version 15.2
-- Dumped by pg_dump version 15.2

-- Started on 2023-04-14 18:43:23

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

--
-- TOC entry 3338 (class 1262 OID 16398)
-- Name: anymind; Type: DATABASE; Schema: -; Owner: postgres
--

CREATE DATABASE anymind WITH TEMPLATE = template0 ENCODING = 'UTF8' LOCALE_PROVIDER = libc LOCALE = 'English_United States.1252';


ALTER DATABASE anymind OWNER TO postgres;

\connect anymind

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

--
-- TOC entry 6 (class 2615 OID 16399)
-- Name: user_service; Type: SCHEMA; Schema: -; Owner: postgres
--

CREATE SCHEMA user_service;


ALTER SCHEMA user_service OWNER TO postgres;

--
-- TOC entry 7 (class 2615 OID 16407)
-- Name: wallet_service; Type: SCHEMA; Schema: -; Owner: postgres
--

CREATE SCHEMA wallet_service;


ALTER SCHEMA wallet_service OWNER TO postgres;

SET default_tablespace = '';

SET default_table_access_method = heap;

--
-- TOC entry 216 (class 1259 OID 16400)
-- Name: user; Type: TABLE; Schema: user_service; Owner: postgres
--

CREATE TABLE user_service."user" (
    user_id bigint NOT NULL,
    email character varying NOT NULL,
    mobile character varying NOT NULL,
    nric character varying NOT NULL,
    first_name character varying NOT NULL,
    last_name character varying NOT NULL,
    street character varying,
    city character varying,
    state character varying,
    zip_code character varying,
    created_date bigint,
    updated_date bigint,
    version integer
);


ALTER TABLE user_service."user" OWNER TO postgres;

--
-- TOC entry 218 (class 1259 OID 16415)
-- Name: wlt_balance_ledger; Type: TABLE; Schema: wallet_service; Owner: postgres
--

CREATE TABLE wallet_service.wlt_balance_ledger (
    balance_ledger_id bigint NOT NULL,
    source_wallet_id bigint NOT NULL,
    destination_wallet_id bigint NOT NULL,
    before numeric,
    after numeric,
    amount numeric,
    transaction_date bigint,
    description character varying,
    version integer,
    created_date bigint,
    updated_date bigint
);


ALTER TABLE wallet_service.wlt_balance_ledger OWNER TO postgres;

--
-- TOC entry 217 (class 1259 OID 16408)
-- Name: wlt_wallet; Type: TABLE; Schema: wallet_service; Owner: postgres
--

CREATE TABLE wallet_service.wlt_wallet (
    wallet_id bigint NOT NULL,
    name character varying,
    balance numeric NOT NULL,
    added_on bigint,
    created_date bigint,
    updated_date bigint,
    version integer
);


ALTER TABLE wallet_service.wlt_wallet OWNER TO postgres;

--
-- TOC entry 3330 (class 0 OID 16400)
-- Dependencies: 216
-- Data for Name: user; Type: TABLE DATA; Schema: user_service; Owner: postgres
--

COPY user_service."user" (user_id, email, mobile, nric, first_name, last_name, street, city, state, zip_code, created_date, updated_date, version) FROM stdin;
434537282990215168	yewintnaing@gmail.com	090878787	dfdff	ye	wint naing	\N	\N	\N	\N	1681415161579	1681415161579	0
434539812495265792	yewintnaing@gmail.com	090878787	dfdff	ye	wint naing	\N	\N	\N	\N	1681415764686	1681415764686	0
\.


--
-- TOC entry 3332 (class 0 OID 16415)
-- Dependencies: 218
-- Data for Name: wlt_balance_ledger; Type: TABLE DATA; Schema: wallet_service; Owner: postgres
--

COPY wallet_service.wlt_balance_ledger (balance_ledger_id, source_wallet_id, destination_wallet_id, before, after, amount, transaction_date, description, version, created_date, updated_date) FROM stdin;
434777384970588160	434539812095265792	434539812695265792	48000	49000	1000	1681472406350	Transaction WalletId[id=434539812695265792]	0	1681472406361	1681472406361
\.


--
-- TOC entry 3331 (class 0 OID 16408)
-- Dependencies: 217
-- Data for Name: wlt_wallet; Type: TABLE DATA; Schema: wallet_service; Owner: postgres
--

COPY wallet_service.wlt_wallet (wallet_id, name, balance, added_on, created_date, updated_date, version) FROM stdin;
434539812095265792	dfdff	44000	1681463167465	1681463167485	1681472270721	12
434539812695265792	dfdff	49000	1681463204959	1681463204983	1681472406354	13
\.


--
-- TOC entry 3183 (class 2606 OID 16406)
-- Name: user user_pk; Type: CONSTRAINT; Schema: user_service; Owner: postgres
--

ALTER TABLE ONLY user_service."user"
    ADD CONSTRAINT user_pk PRIMARY KEY (user_id);


--
-- TOC entry 3187 (class 2606 OID 16421)
-- Name: wlt_balance_ledger bis_wlt_balance_ledger_pk; Type: CONSTRAINT; Schema: wallet_service; Owner: postgres
--

ALTER TABLE ONLY wallet_service.wlt_balance_ledger
    ADD CONSTRAINT bis_wlt_balance_ledger_pk PRIMARY KEY (balance_ledger_id);


--
-- TOC entry 3185 (class 2606 OID 16414)
-- Name: wlt_wallet wlt_wallet_pk; Type: CONSTRAINT; Schema: wallet_service; Owner: postgres
--

ALTER TABLE ONLY wallet_service.wlt_wallet
    ADD CONSTRAINT wlt_wallet_pk PRIMARY KEY (wallet_id);


-- Completed on 2023-04-14 18:43:24

--
-- PostgreSQL database dump complete
--

