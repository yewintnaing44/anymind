## Guide

### Database Setup
### Schema: `user_service`

This schema is used to store information related to users.

## Table: `user_service."user"`

This table stores user information.

| Column Name | Data Type | Nullable | Description |
| --- | --- | --- | --- |
| `user_id` | `bigint` | `NOT NULL` | Unique identifier for each user |
| `email` | `character varying` | `NOT NULL` | User's email address |
| `mobile` | `character varying` | `NOT NULL` | User's mobile number |
| `nric` | `character varying` | `NOT NULL` | User's NRIC (National Registration Identity Card) number |
| `first_name` | `character varying` | `NOT NULL` | User's first name |
| `last_name` | `character varying` | `NOT NULL` | User's last name |
| `street` | `character varying` |  | User's street address |
| `city` | `character varying` |  | User's city of residence |
| `state` | `character varying` |  | User's state of residence |
| `zip_code` | `character varying` |  | User's ZIP code |
| `created_date` | `bigint` |  | Unix timestamp for the creation date |
| `updated_date` | `bigint` |  | Unix timestamp for the last update date |
| `version` | `integer` |  | Version number for the user information |

Note:

- `user_id` column represents the unique identifier for each user in the system.
- `nric` column stores the user's National Registration Identity Card number.
- `created_date` and `updated_date` columns store Unix timestamps in seconds.


### Schema: `wallet_service`

This schema is used to store information related to wallets and their transaction history.

### Schema: `wallet_service`

This schema is used to store information related to wallets and their transaction history.

#### Table: `wallet_service.wlt_wallet`

This table stores wallet information.

| Column Name | Data Type | Nullable | Description |
| --- | --- | --- | --- |
| `wallet_id` | `bigint` | `NOT NULL` | Unique identifier for each wallet |
| `name` | `character varying` |  | Name of the wallet |
| `balance` | `numeric` | `NOT NULL` | Balance of the wallet |
| `added_on` | `bigint` |  | Unix timestamp for when the wallet was added |
| `created_date` | `bigint` |  | Unix timestamp for the creation date |
| `updated_date` | `bigint` |  | Unix timestamp for the last update date |
| `version` | `integer` |  | Version number for the wallet information |

Note:

- `wallet_id` column represents the unique identifier for each wallet in the system.
- `balance` column stores the balance of the wallet, which is always numeric and not nullable.



#### Table: `wallet_service.wlt_balance_ledger`

This table stores the transaction details for wallet balances.

| Column Name | Data Type | Nullable | Description |
| --- | --- | --- | --- |
| `balance_ledger_id` | `bigint` | `NOT NULL` | Unique identifier for each transaction |
| `source_wallet_id` | `bigint` | `NOT NULL` | Identifier for the source wallet |
| `destination_wallet_id` | `bigint` | `NOT NULL` | Identifier for the destination wallet |
| `before` | `numeric` |  | Balance before the transaction |
| `after` | `numeric` |  | Balance after the transaction |
| `amount` | `numeric` |  | Amount of the transaction |
| `transaction_date` | `bigint` |  | Unix timestamp for the transaction date |
| `description` | `character varying` |  | Description of the transaction |
| `version` | `integer` |  | Version number for the transaction |
| `created_date` | `bigint` |  | Unix timestamp for the creation date |
| `updated_date` | `bigint` |  | Unix timestamp for the last update date |

Note:

- `before` and `after` columns represent the wallet balance before and after the transaction respectively.
- `transaction_date`, `created_date`, and `updated_date` columns store Unix timestamps in seconds.
- `description` column allows for a text description of the transaction to be stored.

### Kafka Topics: `user-created-topic` and `transactions`

This document describes the creation of two Kafka topics: `user-created-topic` and `transactions`.

## `user-created-topic`

This topic will be used to publish events related to user creation in the system.

### Topic Configuration

| Configuration | Value                  | Description |
| --- |------------------------| --- |
| `Name` | `user-created-topic`   | The name of the topic |
| `Replication Factor` | `1`                    | The number of replicas for each partition |
| `Partition Count` | `1`                    | The number of partitions for the topic |

# Kafka Topic: `transactions`

This document describes the creation of a Kafka topic: `transactions`.

## Topic Configuration

| Configuration | Value         | Description |
| --- |---------------| --- |
| `Name` | `transactions` | The name of the topic |
| `Replication Factor` | `1`           | The number of replicas for each partition |
| `Partition Count` | `1`           | The number of partitions for the topic |

### Example Usage

To publish a transaction event, you can use the following command:

```sh
kafka-console-producer.sh --broker-list localhost:9092 --topic transactions

{"sourceWalletId": "434539812695265792", "destinationWalletId": "434539812095265792", "amount": 1000, "type": "DEPOSIT"}
```

## System Architecture Overview

This document provides an overview of the system architecture for the wallet service.

### Components

The wallet service consists of the following components:

* User service
* Wallet service
* Credential service
* Kafka

## Workflow

When a client calls the `register` API in the user service, the user service will create a new user account and publish a `user-created-event` to Kafka. The `wallet-consumer` and `credential-service-consumer` will consume this event and create a new wallet account and credential for the user, respectively.

### Example Workflow

1. Client calls the `register` API in the user service with user information.
2. User service creates a new user account.
3. User service publishes a `user-created-event` to Kafka containing the user information.
4. `Wallet-consumer` and `credential-service-consumer` consume the `user-created-event` from Kafka.
5. `Wallet-consumer` creates a new wallet account for the user.
6. `Credential-service-consumer` creates a new credential for the user.
### Example Workflow 2

1. Client calls the `send-balance` API in the wallet service to transfer funds.
2. Wallet service publishes a `transaction` event to Kafka containing the source wallet ID, destination wallet ID, amount, and type of transaction.
3. `Wallet-consumer` consumes the `transaction` event from Kafka.
4. `Wallet-consumer` updates the balance for the source and destination wallets.
5. `Wallet-consumer` adds a transaction history entry for the transaction.


## Kafka Topics

The following Kafka topics are used in the system:

* `user-created-events`
    * Produced by: User service
    * Consumed by: Wallet service, Credential service
    * Description: Contains user information for a newly created user account.
* `transactions`
  * Produced by: Wallet service
  * Consumed by: Wallet service
  * Description: Contains details of a transaction, including the source wallet ID, destination wallet ID, amount, and type of transaction.