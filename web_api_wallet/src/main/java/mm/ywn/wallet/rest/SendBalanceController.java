package mm.ywn.wallet.rest;

import mm.ywn.wallet.application.command.PerformTransactionCommand;
import mm.ywn.wallet.exception.InsufficientBalanceException;
import mm.ywn.wallet.exception.WalletNotFoundException;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import java.math.BigDecimal;

@RestController
public class SendBalanceController {

    public SendBalanceController(PerformTransactionCommand performTransactionCommand) {
        this.performTransactionCommand = performTransactionCommand;
    }

    public record Request(String sourceWalletId, String destinationWalletId, BigDecimal amount) {
    }

    public record Response(String message) {
    }

    private final PerformTransactionCommand performTransactionCommand;

    @PostMapping("/send-balance")
    public Response execute(@RequestBody Request request) throws WalletNotFoundException, InsufficientBalanceException {
        performTransactionCommand.execute(new PerformTransactionCommand.Input(Long.parseLong(request.sourceWalletId()), Long.parseLong(request.destinationWalletId()), request.amount()));
        return new Response("Transaction performed successfully");
    }
}
