package mm.ywn.wallet.rest;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import mm.ywn.common.converter.InstantToLong;
import mm.ywn.common.converter.LongToInstant;
import mm.ywn.wallet.application.query.GetTransactionHistoryListQuery;
import mm.ywn.wallet.exception.WalletNotFoundException;
import mm.ywn.wallet.model.BalanceLedgerId;
import mm.ywn.wallet.model.WalletId;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import java.math.BigDecimal;
import java.time.Instant;
import java.util.List;

@RestController
public class GetBalanceLedgerListController {


    public GetBalanceLedgerListController(GetTransactionHistoryListQuery getTransactionHistoryListQuery) {
        this.getTransactionHistoryListQuery = getTransactionHistoryListQuery;
    }

    public record Request(String walletId, @JsonDeserialize(using = LongToInstant.class) Instant from,
                          @JsonDeserialize(using = LongToInstant.class) Instant to, int page, int size) {
    }


    public record Response(List<BalanceLedger> balanceLedgerList, int page, int size, int total) {
        public record BalanceLedger(String id, String from, String to, BigDecimal before, BigDecimal after,
                                    String description,
                                @JsonSerialize(using = InstantToLong.class) Instant transactionDate,
                                    BigDecimal transactionAmount) {
        }

    }

    private final GetTransactionHistoryListQuery getTransactionHistoryListQuery;

    @PostMapping("/get-balance-ledger-list")
    public ResponseEntity<Response> execute(@RequestBody Request request) throws WalletNotFoundException {

        var output = this.getTransactionHistoryListQuery.execute(new GetTransactionHistoryListQuery.Input(new WalletId(Long.parseLong(request.walletId)), request.from, request.to, request.page, request.size));


        var balanceLedgerList = output.balanceLedgerList().stream().map(balanceLedger -> new Response.BalanceLedger(String.valueOf(balanceLedger.id().id()), String.valueOf(balanceLedger.from().id()), String.valueOf(balanceLedger.to().id()), balanceLedger.before(), balanceLedger.after(), balanceLedger.description(), balanceLedger.transactionDate(), balanceLedger.transactionAmount())).toList();

        return new ResponseEntity<>(new Response(balanceLedgerList, output.page(), output.size(), output.total()), HttpStatus.OK);

    }
}
