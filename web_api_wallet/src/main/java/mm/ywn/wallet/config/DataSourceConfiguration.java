package mm.ywn.wallet.config;


import mm.ywn.common.config.ApplicationConfiguration;
import mm.ywn.common.datasource.PostgresConfiguration;
import mm.ywn.common.datasource.setting.PostgresSetting;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.env.Environment;

@PropertySource("classpath:/postgres_ds.properties")
@Import(value = {ApplicationConfiguration.class, PostgresConfiguration.class})
@PropertySource(value = {"classpath:/postgres_ds.properties"})
@Configuration
public
class DataSourceConfiguration {

    @Autowired
    private Environment env;

    @Bean
    public PostgresSetting defaultPostgresSettings() {

        return new DefaultPostgresSetting(
                this.env.getRequiredProperty("jdbc.db.url"),
                this.env.getRequiredProperty("jdbc.db.username"), this.env.getRequiredProperty("jdbc.db.password"),
                this.env.getRequiredProperty("jdbc.db.driver"),
                Integer.parseInt(this.env.getRequiredProperty("jdbc.db.poolSize")),
                this.env.getRequiredProperty("jdbc.hibernate.dialect"),
                Boolean.parseBoolean(this.env.getRequiredProperty("jdbc.hibernate.show_sql")),
                Boolean.parseBoolean(this.env.getRequiredProperty("jdbc.hibernate.format_sql")),
                this.env.getRequiredProperty("jdbc.hibernate.default_schema"));

    }

//    @Bean(name = "readOnlyPostgresSetting")
//    public PostgresSetting defaultPostgresReadOnlySettings() {
//
//        return new DefaultPostgresSetting(this.env.getRequiredProperty("readOnly.jdbc.db.url"),
//                this.env.getRequiredProperty("readOnly.jdbc.db.username"),
//                this.env.getRequiredProperty("readOnly.jdbc.db.password"),
//                this.env.getRequiredProperty("readOnly.jdbc.db.driver"),
//                Integer.parseInt(this.env.getRequiredProperty("readOnly.jdbc.db.poolSize")),
//                this.env.getRequiredProperty("readOnly.jdbc.hibernate.dialect"),
//                Boolean.parseBoolean(this.env.getRequiredProperty("readOnly.jdbc.hibernate.show_sql")),
//                Boolean.parseBoolean(this.env.getRequiredProperty("readOnly.jdbc.hibernate.format_sql")),
//                this.env.getRequiredProperty("readOnly.jdbc.hibernate.default_schema"));
//
//    }

}