package mm.ywn.common.misc;

public enum Result {

    PROCESSING, SUCCESS, FAIL, CANCELLED;
}
