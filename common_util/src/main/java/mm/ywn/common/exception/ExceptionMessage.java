package mm.ywn.common.exception;

import lombok.AccessLevel;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import mm.ywn.common.misc.LanguageType;
import mm.ywn.common.misc.Text;

import java.util.HashSet;
import java.util.Optional;
import java.util.Set;

@Getter
@EqualsAndHashCode
@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class ExceptionMessage {

    public static ExceptionMessage of(ApplicationException e) {

        ExceptionMessage exceptionMessage = new ExceptionMessage();

        exceptionMessage.exception = e;
        exceptionMessage.code = e.asCode();

        return exceptionMessage;

    }

    protected Exception exception;

    protected String code;

    protected Set<Text> texts = new HashSet<>();

    public void addText(LanguageType language, String description) {

        this.texts.add(new Text(language, description));

    }

    public Optional<Text> getText(LanguageType language) {

        return this.texts.stream().filter((text) -> text.getLanguage().equals(language)).findFirst();

    }

}
