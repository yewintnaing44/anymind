package mm.ywn.common.exception;

import java.util.List;
import java.util.Optional;

import lombok.Getter;

@Getter
public class ApplicationException extends Exception {


    private List<String> params;

    public ApplicationException() {

        super();

    }

    public ApplicationException(List<String> params) {

        this.params = params;

    }

    public String asCode() {

        String exceptionName = this.getClass().getSimpleName();

        return exceptionName.replaceAll(String.format("%s|%s|%s", "(?<=[A-Z])(?=[A-Z][a-z])", "(?<=[^A-Z])(?=[A-Z])",
                "(?<=[A-Za-z])(?=[^A-Za-z])"), " ").replaceAll(" ", "_").toUpperCase();

    }

    public Optional<ExceptionMessage> asMessage() {

        return Optional.empty();

    }

}
