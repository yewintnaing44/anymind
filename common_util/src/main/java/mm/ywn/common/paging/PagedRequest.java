package mm.ywn.common.paging;

import lombok.Getter;

import java.time.Instant;

@Getter
public class PagedRequest {

    private int pageSize;

    private int offset;

    private Instant queryTime;

    public PagedRequest(int pageSize, int offset) {

        super();

        this.pageSize = pageSize;
        this.offset = offset;
        this.queryTime = Instant.now();

    }

}