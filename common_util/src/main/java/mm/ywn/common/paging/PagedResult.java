package mm.ywn.common.paging;

import lombok.Getter;

import java.util.ArrayList;
import java.util.List;

@Getter
public class PagedResult<T> {

    private List<T> result = new ArrayList<>();

    private int pageSize = 100;

    private int offset = 0;

    private int count = 0;

    public PagedResult(List<T> result, int offset, int pageSize, int count) {

        super();

        this.result = result;
        this.offset = offset;
        this.pageSize = pageSize;
        this.count = count;

    }

}