package mm.ywn.user.rest;

import mm.ywn.application.command.RegisterUserCommand;
import mm.ywn.application.handler.UserCommandHandler;
import mm.ywn.user.model.Email;
import mm.ywn.user.model.Mobile;
import mm.ywn.user.model.Nrc;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

@RestController
public class RegisterUserController {

    @Autowired
    public RegisterUserController(UserCommandHandler userCommandHandler) {
        this.userCommandHandler = userCommandHandler;
    }

    public record Request(

            @NotNull @NotEmpty String firstName,
            @NotNull @NotEmpty String lastName,
            @NotNull @NotEmpty String email,

            @NotNull @NotEmpty String nrc,

            @NotNull @NotEmpty String mobile,
            String street,
            String city,
            String state,
            String zipCode

    ) {
    }

    public record Response(String id) {
    }

    private final UserCommandHandler userCommandHandler;

    @PostMapping("/register")
    public ResponseEntity<Response> execute(@RequestBody @Validated Request request) {
        var userId = this.userCommandHandler.handleRegisterCommand(
                new RegisterUserCommand.Input(
                        request.firstName(),
                        request.lastName(),
                        new Nrc(request.nrc()),
                        new Mobile(request.mobile()),
                        new Email(request.email()),
                        new RegisterUserCommand.Input.Address(
                                request.street(),
                                request.city(),
                                request.state(),
                                request.zipCode()
                        )
                ));
        return ResponseEntity.ok(new Response(String.valueOf(userId)));
    }

}
