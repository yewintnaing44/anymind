package mm.ywn.user.config;

import lombok.Getter;
import lombok.Value;
import mm.ywn.common.datasource.setting.PostgresSetting;


public record DefaultPostgresReadOnlySetting(
        String url,
        String username,
        String password,
        String driver,
        int poolSize,
        String dialect,
        boolean showSql,
        boolean formatSql,
        String defaultSchema) implements PostgresSetting {

}
