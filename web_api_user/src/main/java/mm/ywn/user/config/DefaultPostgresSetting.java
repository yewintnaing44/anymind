package mm.ywn.user.config;

import mm.ywn.common.datasource.setting.PostgresSetting;

public record DefaultPostgresSetting(
        String url,
        String username,
        String password, String driver,
        int poolSize,
        String dialect,
        boolean showSql, boolean formatSql,
        String defaultSchema) implements PostgresSetting {

}
