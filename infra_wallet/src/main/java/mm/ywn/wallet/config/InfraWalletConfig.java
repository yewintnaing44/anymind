package mm.ywn.wallet.config;

import mm.ywn.wallet.repository.ReadOnlyWalletRepository;
import mm.ywn.wallet.repository.WalletRepository;
import mm.ywn.wallet.service.WalletQueryService;
import mm.ywn.wallet.service.WalletQueryServiceImpl;
import mm.ywn.wallet.service.WalletService;
import mm.ywn.wallet.service.WalletServiceImpl;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import javax.persistence.Basic;

@Configuration
public class InfraWalletConfig {

    @Bean
    public WalletService walletService(WalletRepository walletRepository, ReadOnlyWalletRepository readOnlyWalletRepository) {
        return new WalletServiceImpl(walletRepository,  readOnlyWalletRepository);
    }

    @Bean
    public WalletQueryService walletQueryService(ReadOnlyWalletRepository readOnlyWalletRepository) {
        return new WalletQueryServiceImpl(readOnlyWalletRepository);
    }
}
