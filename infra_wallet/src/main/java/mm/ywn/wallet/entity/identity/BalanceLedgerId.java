package mm.ywn.wallet.entity.identity;


import lombok.EqualsAndHashCode;
import lombok.Getter;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import java.io.Serializable;

@Embeddable
@Getter
@EqualsAndHashCode
public class BalanceLedgerId implements Serializable {
    @Column(name = "balance_ledger_id")
    protected Long value;

    public BalanceLedgerId() {

    }

    public BalanceLedgerId(Long id) {
        this.value = id;
    }
}
