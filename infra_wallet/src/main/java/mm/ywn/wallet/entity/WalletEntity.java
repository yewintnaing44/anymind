package mm.ywn.wallet.entity;

import lombok.Getter;
import lombok.NoArgsConstructor;
import mm.ywn.common.persistence.jpa.JpaEntity;
import mm.ywn.common.persistence.jpa.JpaInstantConverter;
import mm.ywn.wallet.entity.identity.WalletId;

import javax.persistence.*;
import java.math.BigDecimal;
import java.time.Instant;
import java.util.HashSet;
import java.util.Set;

@Entity
@Getter
@NoArgsConstructor
@Table(name = "wlt_wallet")
public class WalletEntity extends JpaEntity {

    @EmbeddedId
    private WalletId id;

    @Column(name = "added_on")
    @Convert(converter = JpaInstantConverter.class)
    private Instant addedOn;

    @Column(name = "balance")
    private BigDecimal balance;

    @Column(name = "name")
    private String name;

    @OneToMany(mappedBy = "sourceWallet", cascade = CascadeType.ALL)
    private Set<BalanceLedgerEntity> sourceTransactions = new HashSet<>();

    @OneToMany(mappedBy = "destinationWallet")
    private Set<BalanceLedgerEntity> destinationTransactions = new HashSet<>();

    public WalletEntity(long id, BigDecimal balance, String name) {
        this.id = new WalletId(id);
        this.balance = balance;
        this.name = name;
        this.addedOn = Instant.now();
    }

    public void updateBalance(BigDecimal balance) {
        this.balance = balance;
    }

    public void addTransactionList(Set<BalanceLedgerEntity> balanceLedgerEntities) {
        this.sourceTransactions.addAll(balanceLedgerEntities);
    }

}
