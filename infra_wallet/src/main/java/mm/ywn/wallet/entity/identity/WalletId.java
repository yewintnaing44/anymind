package mm.ywn.wallet.entity.identity;


import lombok.EqualsAndHashCode;
import lombok.Getter;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import java.io.Serializable;

@Embeddable
@Getter
@EqualsAndHashCode
public class WalletId implements Serializable {
    @Column(name = "wallet_id")
    protected Long value;

    public WalletId() {

    }

    public WalletId(Long id) {
        this.value = id;
    }
}
