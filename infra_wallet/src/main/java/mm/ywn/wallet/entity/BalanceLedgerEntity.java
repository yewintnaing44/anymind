package mm.ywn.wallet.entity;

import lombok.Getter;
import lombok.NoArgsConstructor;
import mm.ywn.common.persistence.jpa.JpaEntity;
import mm.ywn.common.persistence.jpa.JpaInstantConverter;
import mm.ywn.wallet.entity.identity.BalanceLedgerId;

import javax.persistence.*;
import java.math.BigDecimal;
import java.time.Instant;

@Getter
@NoArgsConstructor
@Entity
@Table(name = "wlt_balance_ledger")
public class BalanceLedgerEntity extends JpaEntity {

    @EmbeddedId
    private BalanceLedgerId id;

    @Column(name = "source_wallet_id", insertable = false, updatable = false)
    private long sourceWalletId;

    @Column(name = "destination_wallet_id", insertable = false, updatable = false)
    private long destinationWalletId;

    @JoinColumn(name = "source_wallet_id")
    @ManyToOne
    private WalletEntity sourceWallet;

    @JoinColumn(name = "destination_wallet_id")
    @ManyToOne
    private WalletEntity destinationWallet;

    @Column(name = "amount")
    private BigDecimal amount;

    @Column(name = "before")
    private BigDecimal before;

    @Column(name = "after")
    private BigDecimal after;

    @Column(name = "description")
    private String description;

    @Column(name = "transaction_date")
    @Convert(converter = JpaInstantConverter.class)
    private Instant transactionDate;
    public BalanceLedgerEntity(long id, WalletEntity sourceWallet, WalletEntity destinationWallet, BigDecimal amount, BigDecimal before, BigDecimal after, String description, Instant transactionDate) {
        this.id = new BalanceLedgerId(id);
        this.sourceWallet = sourceWallet;
        this.destinationWallet = destinationWallet;
        this.amount = amount;
        this.before = before;
        this.after = after;
        this.description = description;
        this.transactionDate = transactionDate;
    }
}
