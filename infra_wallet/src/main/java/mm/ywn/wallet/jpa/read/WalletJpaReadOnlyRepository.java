package mm.ywn.wallet.jpa.read;

import mm.ywn.common.persistence.ReadOnlyRepository;
import mm.ywn.wallet.entity.WalletEntity;
import mm.ywn.wallet.entity.identity.WalletId;
import org.springframework.data.jpa.repository.JpaRepository;

@ReadOnlyRepository
public interface WalletJpaReadOnlyRepository extends JpaRepository<WalletEntity, WalletId> {
}
