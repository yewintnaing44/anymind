package mm.ywn.wallet.jpa.filters;

import com.querydsl.core.types.dsl.BooleanExpression;
import mm.ywn.wallet.entity.QBalanceLedgerEntity;
import mm.ywn.wallet.entity.identity.BalanceLedgerId;
import mm.ywn.wallet.entity.identity.WalletId;

import java.time.Instant;

public class BalanceLedgerFilter {

    public BooleanExpression notWithBalanceLedgerId(BalanceLedgerId balanceLedgerId) {
        return QBalanceLedgerEntity.balanceLedgerEntity.id.ne(balanceLedgerId);
    }

    public static BooleanExpression withWalletId(WalletId walletId) {
        return QBalanceLedgerEntity.balanceLedgerEntity.sourceWallet.id.eq(walletId)
                .or(QBalanceLedgerEntity.balanceLedgerEntity.destinationWallet.id.eq(walletId));
    }

    public static BooleanExpression withStartingFrom(Instant from) {
        return QBalanceLedgerEntity.balanceLedgerEntity.transactionDate.goe(from);
    }

    public static BooleanExpression withUntil(Instant to) {
        return QBalanceLedgerEntity.balanceLedgerEntity.transactionDate.loe(to);
    }


}
