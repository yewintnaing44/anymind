package mm.ywn.wallet.jpa.read;

import mm.ywn.wallet.entity.BalanceLedgerEntity;
import mm.ywn.wallet.entity.identity.BalanceLedgerId;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.querydsl.QuerydslPredicateExecutor;
import org.springframework.stereotype.Repository;

@Repository
public interface BalanceLedgerJpaReadOnlyRepository extends JpaRepository<BalanceLedgerEntity, BalanceLedgerId>, QuerydslPredicateExecutor<BalanceLedgerEntity> {
}
