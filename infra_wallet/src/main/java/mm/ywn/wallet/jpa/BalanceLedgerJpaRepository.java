package mm.ywn.wallet.jpa;

import mm.ywn.wallet.entity.BalanceLedgerEntity;
import mm.ywn.wallet.entity.WalletEntity;
import mm.ywn.wallet.entity.identity.BalanceLedgerId;
import mm.ywn.wallet.entity.identity.WalletId;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface BalanceLedgerJpaRepository extends JpaRepository<BalanceLedgerEntity, BalanceLedgerId> {
}
