package mm.ywn.wallet.repository;

import mm.ywn.wallet.entity.BalanceLedgerEntity;
import mm.ywn.wallet.entity.WalletEntity;
import mm.ywn.wallet.entity.identity.WalletId;
import mm.ywn.wallet.exception.WalletNotFoundException;
import mm.ywn.wallet.jpa.WalletJpaRepository;
import mm.ywn.wallet.jpa.read.WalletJpaReadOnlyRepository;
import mm.ywn.wallet.model.Wallet;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import java.util.stream.Collectors;

@Component
public class WalletRepositoryImpl implements WalletRepository {

    private final WalletJpaRepository walletJpaRepository;

    private final WalletJpaReadOnlyRepository walletJpaReadOnlyRepository;

    public WalletRepositoryImpl(WalletJpaRepository walletJpaRepository, WalletJpaReadOnlyRepository walletJpaReadOnlyRepository) {
        this.walletJpaRepository = walletJpaRepository;
        this.walletJpaReadOnlyRepository = walletJpaReadOnlyRepository;
    }

    @Override
    @Transactional
    public long createWallet(Wallet wallet) {

        var walletEntity = new WalletEntity(wallet.getId().id(), wallet.getBalance(), wallet.getName());

        this.walletJpaRepository.saveAndFlush(walletEntity);

        return wallet.getId().id();
    }

    @Override
    @Transactional
    public void update(Wallet wallet) throws WalletNotFoundException {

        var balanceLedgerList = wallet.getBalanceLedgerList();

        var optionalWalletEntity = this.walletJpaRepository.findById(new WalletId(wallet.getId().id()));

        if (optionalWalletEntity.isEmpty()) {
            throw new WalletNotFoundException("Wallet not found");
        }

        var walletEntity = optionalWalletEntity.get();

        walletEntity.updateBalance(wallet.getBalance());

        var destinationWalletList = this.walletJpaReadOnlyRepository.findAllById(balanceLedgerList.stream().map(balanceLedger -> new WalletId(balanceLedger.getWallet().getId().id())).collect(Collectors.toSet()));
        var sourceWalletList = this.walletJpaReadOnlyRepository.findAllById(balanceLedgerList.stream().map(balanceLedger -> new WalletId(balanceLedger.getFrom().getId().id())).collect(Collectors.toSet()));

        var balanceLedgerEntityList = balanceLedgerList.stream().map(balanceLedger ->
                new BalanceLedgerEntity(
                        balanceLedger.getBalanceLedgerId().id(),
                        sourceWalletList.stream().filter(walletEntity1 -> walletEntity1.getId().getValue() == balanceLedger.getFrom().getId().id()).findFirst().get(),
                        destinationWalletList.stream().filter(walletEntity1 -> walletEntity1.getId().getValue() == balanceLedger.getWallet().getId().id()).findFirst().get(),
                        balanceLedger.getTransactionAmount(),
                        balanceLedger.getBefore(),
                        balanceLedger.getAfter(),
                        balanceLedger.getDescription(),
                        balanceLedger.getTransactionDate())
        ).collect(Collectors.toSet());

        walletEntity.addTransactionList(balanceLedgerEntityList);


        this.walletJpaRepository.save(walletEntity);

    }
}
