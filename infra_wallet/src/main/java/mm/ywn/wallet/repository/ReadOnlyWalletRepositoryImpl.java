package mm.ywn.wallet.repository;

import com.querydsl.core.BooleanBuilder;
import mm.ywn.common.paging.PagedRequest;
import mm.ywn.common.paging.PagedResult;
import mm.ywn.wallet.entity.BalanceLedgerEntity;
import mm.ywn.wallet.exception.WalletNotFoundException;
import mm.ywn.wallet.jpa.filters.BalanceLedgerFilter;
import mm.ywn.wallet.jpa.read.BalanceLedgerJpaReadOnlyRepository;
import mm.ywn.wallet.jpa.read.WalletJpaReadOnlyRepository;
import mm.ywn.wallet.model.BalanceLedger;
import mm.ywn.wallet.model.BalanceLedgerId;
import mm.ywn.wallet.model.Wallet;
import mm.ywn.wallet.model.WalletId;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Component;

import java.time.Instant;
import java.util.List;

@Component
public class ReadOnlyWalletRepositoryImpl implements ReadOnlyWalletRepository {

    private final WalletJpaReadOnlyRepository walletJpaReadOnlyRepository;

    private final BalanceLedgerJpaReadOnlyRepository balanceLedgerJpaReadOnlyRepository;

    public ReadOnlyWalletRepositoryImpl(WalletJpaReadOnlyRepository walletJpaReadOnlyRepository, BalanceLedgerJpaReadOnlyRepository balanceLedgerJpaReadOnlyRepository) {
        this.walletJpaReadOnlyRepository = walletJpaReadOnlyRepository;
        this.balanceLedgerJpaReadOnlyRepository = balanceLedgerJpaReadOnlyRepository;
    }

    @Override
    public Wallet getWalletById(WalletId walletId) throws WalletNotFoundException {

        var wallet = findById(walletId);

        if (wallet == null) {
            throw new WalletNotFoundException("Wallet not found");
        }

        return wallet;
    }

    @Override
    public Wallet findById(WalletId walletId) {
        var optionalWallet = this.walletJpaReadOnlyRepository.findById(new mm.ywn.wallet.entity.identity.WalletId(walletId.id()));

        if (optionalWallet.isEmpty()) {
            return null;
        }

        var wallet = optionalWallet.get();

        return new Wallet(new WalletId(wallet.getId().getValue()), wallet.getName(), wallet.getBalance());

    }

    @Override
    public List<Wallet> getAllWallets() {
        return null;
    }

    @Override
    public PagedResult<BalanceLedger> getBalanceLedgerHistory(Instant to, Instant from, WalletId walletId, PagedRequest pagedRequest) {

        var builder = new BooleanBuilder();

        if (from != null && to != null) {
            builder.and(BalanceLedgerFilter.withStartingFrom(from).and(BalanceLedgerFilter.withUntil(to)));
        }


        if (walletId != null) {
            builder.and(BalanceLedgerFilter.withWalletId(new mm.ywn.wallet.entity.identity.WalletId(walletId.id())));
        }

        var pageable = PageRequest.of(
                pagedRequest.getPageSize(), pagedRequest.getOffset(), Sort.by("transactionDate").descending()
        );

        Page<BalanceLedgerEntity> page = this.balanceLedgerJpaReadOnlyRepository.findAll(builder, pageable);

        return new PagedResult<BalanceLedger>(
                page.getContent().stream().map(balanceLedgerEntity ->
                        new BalanceLedger(
                                new BalanceLedgerId(balanceLedgerEntity.getId().getValue()),
                                new Wallet(
                                        new WalletId(balanceLedgerEntity.getSourceWalletId()),
                                        balanceLedgerEntity.getSourceWallet().getName(),
                                        balanceLedgerEntity.getSourceWallet().getBalance()),
                                new Wallet(
                                        new WalletId(balanceLedgerEntity.getDestinationWalletId()),
                                        balanceLedgerEntity.getDestinationWallet().getName(),
                                        balanceLedgerEntity.getDestinationWallet().getBalance()),
                                balanceLedgerEntity.getAmount(),
                                balanceLedgerEntity.getBefore(),
                                balanceLedgerEntity.getAfter(),
                                balanceLedgerEntity.getDescription(),
                                balanceLedgerEntity.getTransactionDate()
                                )).toList(), pagedRequest.getOffset(), pagedRequest.getPageSize(), (int) page.getTotalElements());

    }
}
