package mm.ywn.wallet;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.kafka.annotation.EnableKafka;

@SpringBootApplication
@ComponentScan(basePackages = {"mm.ywn.*"})
@EnableKafka
public class WalletConsumerApplication {

    public static void main(String[] args) {
        SpringApplication.run(WalletConsumerApplication.class, args);
    }

}