package mm.ywn.wallet.consumer;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import mm.ywn.application.command.implementation.RegisterUserCommandImpl;
import mm.ywn.user.event.UserCreatedEvent;
import mm.ywn.wallet.application.command.CreateWalletCommand;
import mm.ywn.wallet.event.TransactionCreatedEvent;
import mm.ywn.wallet.exception.InvalidTransactionTypeException;
import mm.ywn.wallet.exception.NegativeAmountException;
import mm.ywn.wallet.exception.WalletNotFoundException;
import mm.ywn.wallet.model.WalletId;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;

@Service
public class UserCreatedEventConsumer {

    private final CreateWalletCommand createWalletCommand;

    private final static Logger LOG = LoggerFactory.getLogger(UserCreatedEventConsumer.class);

    public UserCreatedEventConsumer(CreateWalletCommand createWalletCommand) {
        this.createWalletCommand = createWalletCommand;
    }


    @KafkaListener(topics = "user-created-topic", groupId = "wallet-consumer")
    public void consume(String user) {
        // Process the transaction
        LOG.info("User: {}", user);
        ObjectMapper objectMapper = new ObjectMapper();

        objectMapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);

        try {
            var userEvent = objectMapper.readValue(user, UserCreatedEvent.class);

            this.createWalletCommand.execute(
                    new CreateWalletCommand.Input(
                            new WalletId(Long.parseLong(userEvent.id())),
                            userEvent.email(),
                            BigDecimal.ZERO
                    ));

        } catch (JsonProcessingException e) {
            throw new RuntimeException(e);
        }

    }

}
