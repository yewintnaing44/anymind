package mm.ywn.wallet.consumer;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import mm.ywn.wallet.application.command.SaveTransactionCommand;
import mm.ywn.wallet.event.TransactionCreatedEvent;
import mm.ywn.wallet.exception.InvalidTransactionTypeException;
import mm.ywn.wallet.exception.NegativeAmountException;
import mm.ywn.wallet.exception.WalletNotFoundException;
import mm.ywn.wallet.model.WalletId;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.stereotype.Service;

@Service
public class TransactionConsumer {

    private final SaveTransactionCommand saveTransactionCommand;

    public TransactionConsumer(SaveTransactionCommand saveTransactionCommand) {
        this.saveTransactionCommand = saveTransactionCommand;
    }


    @KafkaListener(topics = "transactions", groupId = "wallet-consumer")
    public void consume(String transaction) {
        // Process the transaction

        ObjectMapper objectMapper = new ObjectMapper();

        objectMapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);

        try {
            var transactionEvent = objectMapper.readValue(transaction, TransactionCreatedEvent.class);

            this.saveTransactionCommand.execute(
                    new SaveTransactionCommand.Input(
                            new WalletId(Long.parseLong(transactionEvent.sourceWalletId())),
                            new WalletId(Long.parseLong(transactionEvent.destinationWalletId())),
                            transactionEvent.amount(),
                            transactionEvent.type())
            );

        } catch (JsonProcessingException | NegativeAmountException | WalletNotFoundException |
                 InvalidTransactionTypeException e) {
            throw new RuntimeException(e);
        }

    }

}
