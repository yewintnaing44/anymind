package mm.ywn.wallet.application.command;

import mm.ywn.wallet.exception.InsufficientBalanceException;
import mm.ywn.wallet.exception.WalletNotFoundException;

import java.math.BigDecimal;

public interface PerformTransactionCommand {
    
    public record Input(long sourceWalletId, long destinationWalletId, BigDecimal amount) {
    }
    
    public record Output(){}
    
    public Output execute(Input input) throws WalletNotFoundException, InsufficientBalanceException;
}
