package mm.ywn.wallet.application.command;

import mm.ywn.wallet.exception.InvalidTransactionTypeException;
import mm.ywn.wallet.exception.NegativeAmountException;
import mm.ywn.wallet.exception.WalletNotFoundException;
import mm.ywn.wallet.model.TransactionType;
import mm.ywn.wallet.model.WalletId;

import java.math.BigDecimal;

public interface SaveTransactionCommand {

    public Output execute(Input input) throws NegativeAmountException, WalletNotFoundException, InvalidTransactionTypeException;

    public record Input(WalletId sourceWalletId, WalletId destinationWalletId, BigDecimal amount, TransactionType transactionType) {
    }

    public record Output() {
    }


}
