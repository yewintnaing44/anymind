package mm.ywn.wallet.application.query;


import mm.ywn.wallet.exception.WalletNotFoundException;
import mm.ywn.wallet.model.BalanceLedgerId;
import mm.ywn.wallet.model.WalletId;

import java.math.BigDecimal;
import java.time.Instant;
import java.util.List;

public interface GetTransactionHistoryListQuery {

    public record Input(WalletId walletId, Instant from, Instant to, int page, int size) {
    }

    public record Ouput(List<BalanceLedger> balanceLedgerList, int page, int size, int total) {
        public record BalanceLedger(BalanceLedgerId id, WalletId from, WalletId to, BigDecimal before, BigDecimal after,
                                    String description, Instant transactionDate, BigDecimal transactionAmount) {
        }

    }

    public Ouput execute(Input input) throws WalletNotFoundException;
}
