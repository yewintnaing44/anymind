package mm.ywn.wallet.application.query.implementation;

import mm.ywn.common.paging.PagedRequest;
import mm.ywn.wallet.application.query.GetTransactionHistoryListQuery;
import mm.ywn.wallet.exception.WalletNotFoundException;
import mm.ywn.wallet.service.WalletQueryService;
import org.springframework.stereotype.Service;

@Service
public class GetTransactionHistoryListQueryImpl implements GetTransactionHistoryListQuery {

    private final WalletQueryService walletQueryService;

    public GetTransactionHistoryListQueryImpl(WalletQueryService walletQueryService) {
        this.walletQueryService = walletQueryService;
    }

    @Override
    public Ouput execute(Input input) throws WalletNotFoundException {

        var output = this.walletQueryService.getBalanceLedgerHistory(input.to(), input.from(), input.walletId(), new PagedRequest(input.page(), input.size()));

        var balanceLedgerList = output.getResult().stream().map(balanceLedger -> new Ouput.BalanceLedger(balanceLedger.getBalanceLedgerId(), balanceLedger.getFrom().getId(), balanceLedger.getWallet().getId(), balanceLedger.getBefore(), balanceLedger.getAfter(), balanceLedger.getDescription(), balanceLedger.getTransactionDate(), balanceLedger.getTransactionAmount())).toList();

        return new Ouput(balanceLedgerList, output.getPageSize(), output.getOffset(), output.getCount());
    }
}
