package mm.ywn.wallet.application.command.implementation;

import mm.ywn.wallet.application.command.CreateWalletCommand;
import mm.ywn.wallet.service.WalletService;
import org.springframework.stereotype.Service;

@Service
public class CreateWalletCommandImpl implements CreateWalletCommand {

    private final WalletService walletService;

    public CreateWalletCommandImpl(WalletService walletService) {
        this.walletService = walletService;
    }

    @Override
    public Output execute(Input input) {

        var walletId = this.walletService.createWallet(input.walletId(),input.name(), input.initialBalance());
        return new Output(walletId);
    }
}
