package mm.ywn.wallet.application.command.implementation;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import mm.ywn.wallet.application.command.PerformTransactionCommand;
import mm.ywn.wallet.event.TransactionCreatedEvent;
import mm.ywn.wallet.exception.InsufficientBalanceException;
import mm.ywn.wallet.exception.WalletNotFoundException;
import mm.ywn.wallet.model.TransactionType;
import mm.ywn.wallet.model.WalletId;
import mm.ywn.wallet.service.WalletQueryService;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.stereotype.Service;

@Service
public class PerformTransactionCommandImpl implements PerformTransactionCommand {

    private final WalletQueryService walletQueryService;

    private final KafkaTemplate<String, String> kafkaTemplate;

    public PerformTransactionCommandImpl(WalletQueryService walletQueryService, KafkaTemplate<String, String> kafkaTemplate) {
        this.walletQueryService = walletQueryService;
        this.kafkaTemplate = kafkaTemplate;
    }


    @Override
    public Output execute(Input input) throws WalletNotFoundException, InsufficientBalanceException {

        var sourceWallet = this.walletQueryService.getWalletById(new WalletId(input.sourceWalletId()));
        var destinationWallet = this.walletQueryService.getWalletById(new WalletId(input.destinationWalletId()));

        if (sourceWallet.balance().compareTo(input.amount()) < 0) {
            throw new InsufficientBalanceException("Insufficient balance");
        }

        var sourceTransactionEvent = new TransactionCreatedEvent(String.valueOf(sourceWallet.walletId().id()), String.valueOf(destinationWallet.walletId().id()), input.amount(), TransactionType.WITHDRAW);
        ObjectMapper objectMapper = new ObjectMapper();

        String sourceTransactionEventJson;
        try {
            sourceTransactionEventJson = objectMapper.writeValueAsString(sourceTransactionEvent);
        } catch (JsonProcessingException e) {
            throw new RuntimeException(e);
        }
        kafkaTemplate.send("transactions", sourceTransactionEventJson);

        var destinationTransactionEvent = new TransactionCreatedEvent(String.valueOf(destinationWallet.walletId().id()), String.valueOf(sourceWallet.walletId().id()), input.amount(), TransactionType.DEPOSIT);
        String destinationTransactionEventJson;
        try {
            destinationTransactionEventJson = objectMapper.writeValueAsString(destinationTransactionEvent);
        } catch (JsonProcessingException e) {
            throw new RuntimeException(e);
        }
        kafkaTemplate.send("transactions", destinationTransactionEventJson);


        return new Output();
    }
}
