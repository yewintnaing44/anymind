package mm.ywn.wallet.application.command.implementation;

import mm.ywn.wallet.application.command.SaveTransactionCommand;
import mm.ywn.wallet.exception.InvalidTransactionTypeException;
import mm.ywn.wallet.exception.NegativeAmountException;
import mm.ywn.wallet.exception.WalletNotFoundException;
import mm.ywn.wallet.model.WalletId;
import mm.ywn.wallet.service.WalletService;
import org.springframework.stereotype.Service;

@Service
public class SaveTransactionCommandImpl implements SaveTransactionCommand {

    private final WalletService walletService;

    public SaveTransactionCommandImpl(WalletService walletService) {
        this.walletService = walletService;
    }

    @Override
    public Output execute(Input input) throws NegativeAmountException, WalletNotFoundException, InvalidTransactionTypeException {

        this.walletService.performTransaction(
                input.sourceWalletId(),
                input.destinationWalletId(),
                input.amount(),
                "Transaction " + input.sourceWalletId(),
                input.transactionType());
        return new Output();
    }
}
