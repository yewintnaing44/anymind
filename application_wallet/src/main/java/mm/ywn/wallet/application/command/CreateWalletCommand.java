package mm.ywn.wallet.application.command;


import mm.ywn.wallet.model.WalletId;

import java.math.BigDecimal;

public interface CreateWalletCommand {

    public Output execute(Input input);

    public record Input(WalletId walletId, String name, BigDecimal initialBalance) {
    }

    public record Output(long walletId) {
    }


}
