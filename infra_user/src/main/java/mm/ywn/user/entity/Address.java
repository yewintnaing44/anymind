package mm.ywn.user.entity;


import lombok.Getter;
import lombok.NoArgsConstructor;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import java.util.Objects;

@Embeddable
@Getter
public  class Address {
        @Column(name = "street")
        private final String street;
        @Column(name = "city")
        private final String city;
        @Column(name = "state")
        private final String state;
        @Column(name = "zip_code")
        private final String zipCode;

        public Address(
                String street,
                String city,
                String state,
                String zipCode) {
                this.street = street;
                this.city = city;
                this.state = state;
                this.zipCode = zipCode;
        }

        public Address() {
                this(null, null, null, null);
        }

        @Column(name = "street")
        public String street() {
                return street;
        }


        @Override
        public boolean equals(Object obj) {
                if (obj == this) return true;
                if (obj == null || obj.getClass() != this.getClass()) return false;
                var that = (Address) obj;
                return Objects.equals(this.street, that.street) &&
                        Objects.equals(this.city, that.city) &&
                        Objects.equals(this.state, that.state) &&
                        Objects.equals(this.zipCode, that.zipCode);
        }

        @Override
        public int hashCode() {
                return Objects.hash(street, city, state, zipCode);
        }

        @Override
        public String toString() {
                return "Address[" +
                        "street=" + street + ", " +
                        "city=" + city + ", " +
                        "state=" + state + ", " +
                        "zipCode=" + zipCode + ']';
        }

}
