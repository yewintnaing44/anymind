package mm.ywn.user.entity.identity;


import lombok.EqualsAndHashCode;
import lombok.Getter;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.persistence.Id;
import java.io.Serializable;

@Embeddable
@Getter
@EqualsAndHashCode
public class UserId implements Serializable {
    @Column(name = "user_id")
    protected Long value;

    public UserId() {

    }

    public UserId(Long id) {
        this.value = id;
    }
}
