package mm.ywn.user.entity;

import lombok.AccessLevel;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import mm.ywn.common.persistence.identifier.Email;
import mm.ywn.common.persistence.identifier.Mobile;
import mm.ywn.common.persistence.identifier.Nric;
import mm.ywn.common.persistence.jpa.JpaEntity;
import mm.ywn.user.entity.identity.UserId;

import javax.persistence.*;

@Entity
@Table(name = "user")
@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
@EqualsAndHashCode(onlyExplicitlyIncluded = true, callSuper = false)
public class UserEntity extends JpaEntity {

    @EmbeddedId
    private UserId id;

    @Column(name = "first_name")
    private String firstName;

    @Column(name = "last_name")
    private String lastName;


    @Column(name = "email")
    @Convert(converter = Email.JpaConverter.class)
    private Email email;


    @Column(name = "mobile")
    @Convert(converter = Mobile.JpaConverter.class)
    private Mobile mobile;

    @Column(name = "nric")
    @Convert(converter = Nric.JpaConverter.class)
    private Nric nric;

    @Embedded
    private Address address;

    public UserEntity(
            long id,
            String firstName,
            String lastName,
            Email email,
            Mobile mobile,
            Nric nric,
            Address address
    ) {
        this.id = new UserId(id);
        this.firstName = firstName;
        this.lastName = lastName;
        this.email = email;
        this.mobile = mobile;
        this.nric = nric;
        this.address = address;
    }

    public void updateUser(String firstName, String lastName, Email email, Mobile mobile, Nric nric, Address address) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.email = email;
        this.mobile = mobile;
        this.nric = nric;
        this.address = address;
    }
}
