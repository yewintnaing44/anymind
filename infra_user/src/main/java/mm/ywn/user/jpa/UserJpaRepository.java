package mm.ywn.user.jpa;

import mm.ywn.user.entity.UserEntity;
import mm.ywn.user.entity.identity.UserId;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface UserJpaRepository extends JpaRepository<UserEntity, UserId> {
}
