package mm.ywn.user.jpa.read;

import mm.ywn.common.persistence.ReadOnlyRepository;
import mm.ywn.user.entity.UserEntity;
import mm.ywn.user.entity.identity.UserId;
import org.springframework.data.jpa.repository.JpaRepository;

@ReadOnlyRepository
public interface UserJpaReadOnlyRepository extends JpaRepository<UserEntity, UserId> {
}
