package mm.ywn.user.mapper;

import mm.ywn.user.entity.UserEntity;
import mm.ywn.user.model.Email;
import mm.ywn.user.model.Mobile;
import mm.ywn.user.model.Nrc;
import mm.ywn.user.model.User;
import org.springframework.stereotype.Component;

@Component
public class UserMapper {
    public User toDto(UserEntity userEntity) {
        var user = new User(
                userEntity.getId().getValue(),
                userEntity.getFirstName(),
                userEntity.getLastName(),
                new Nrc(userEntity.getNric().getValue()),
                new Mobile(userEntity.getMobile().getValue()),
                new Email(userEntity.getEmail().getValue()));

        if (userEntity.getAddress() != null) {
            user.setAddress(userEntity.getAddress().street(), userEntity.getAddress().getCity(), userEntity.getAddress().getState(), userEntity.getAddress().getZipCode());
        }

        return user;
    }
}
