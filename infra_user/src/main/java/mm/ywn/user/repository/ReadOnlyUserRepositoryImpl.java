package mm.ywn.user.repository;

import mm.ywn.user.entity.identity.UserId;
import mm.ywn.user.jpa.read.UserJpaReadOnlyRepository;
import mm.ywn.user.mapper.UserMapper;
import mm.ywn.user.model.User;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class ReadOnlyUserRepositoryImpl implements ReadOnlyUserRepository{

    private final UserJpaReadOnlyRepository userJpaReadOnlyRepository;

    private final UserMapper userMapper;

    public ReadOnlyUserRepositoryImpl(UserJpaReadOnlyRepository userJpaReadOnlyRepository, UserMapper userMapper) {
        this.userJpaReadOnlyRepository = userJpaReadOnlyRepository;
        this.userMapper = userMapper;
    }

    @Override
    public User findById(mm.ywn.user.model.UserId id) {
        return this.userJpaReadOnlyRepository.findById(new UserId(id.id())).map(this.userMapper::toDto).orElse(null);
    }

    @Override
    public List<User> findAll() {
        return this.userJpaReadOnlyRepository.findAll().stream().map(this.userMapper::toDto).toList();
    }
}
