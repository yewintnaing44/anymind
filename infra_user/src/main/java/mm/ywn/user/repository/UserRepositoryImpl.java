package mm.ywn.user.repository;


import mm.ywn.common.exception.NoRollbackApplicationException;
import mm.ywn.common.persistence.identifier.Email;
import mm.ywn.common.persistence.identifier.Mobile;
import mm.ywn.common.persistence.identifier.Nric;
import mm.ywn.user.entity.Address;
import mm.ywn.user.entity.UserEntity;
import mm.ywn.user.entity.identity.UserId;
import mm.ywn.user.exception.UserNotFoundException;
import mm.ywn.user.jpa.UserJpaRepository;
import mm.ywn.user.model.User;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;


@Component
public class UserRepositoryImpl implements UserRepository {

    private final UserJpaRepository userJpaRepository;


    public UserRepositoryImpl(UserJpaRepository userJpaRepository) {
        this.userJpaRepository = userJpaRepository;
    }

    @Override
    @Transactional(rollbackFor = Exception.class, noRollbackFor = NoRollbackApplicationException.class)
    public void save(User user) {
        var userEntity = new UserEntity(
                user.getUserId().id(),
                user.getFirstName(),
                user.getLastName(),
                new Email(user.getEmail().id()),
                new Mobile(user.getMobile().id()),
                new Nric(user.getNrc().id()),
               new Address(user.getAddress().street(), user.getAddress().city(), user.getAddress().state(), user.getAddress().zipCode())
        );

        userJpaRepository.save(userEntity);
    }

//    @Override
//    @Transactional
//    public void update(User user) throws ApplicationException {
//
//        var opUserEntity = this.userJpaRepository.findById(new UserId(user.getUserId().id()));
//
//        if (opUserEntity.isEmpty()) {
//            throw new UserNotFoundException();
//        }
//
//        var userEntity = opUserEntity.get();
//
//
//        Address address = null;
//
//        if (user.getAddress() != null) {
//            address = new Address(user.getAddress().street(), user.getAddress().city(), user.getAddress().state(), user.getAddress().zipCode());
//        }
//
//        userEntity.updateUser(
//                user.getFirstName(),
//                user.getLastName(),
//                new Email(user.getEmail().id()),
//                new Mobile(user.getMobile().id()),
//                new Nric(user.getNrc().id()),
//                address);
//
//        this.userJpaRepository.saveAndFlush(userEntity);
//
//    }
}
