package mm.ywn.user.config;

import mm.ywn.user.repository.ReadOnlyUserRepository;
import mm.ywn.user.repository.UserRepository;
import mm.ywn.user.service.UserQueryService;
import mm.ywn.user.service.UserQueryServiceImpl;
import mm.ywn.user.service.UserService;
import mm.ywn.user.service.UserServiceImpl;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class InfraUserConfig {

    @Bean
    public UserService userService(UserRepository userRepository, ReadOnlyUserRepository readOnlyUserRepository) {
        return new UserServiceImpl(userRepository, readOnlyUserRepository);
    }

    @Bean
    public UserQueryService userQueryService(ReadOnlyUserRepository readOnlyUserRepository) {
        return new UserQueryServiceImpl(readOnlyUserRepository);
    }
}
