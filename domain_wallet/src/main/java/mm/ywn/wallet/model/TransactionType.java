package mm.ywn.wallet.model;

public enum TransactionType {
    WITHDRAW,
    DEPOSIT
}
