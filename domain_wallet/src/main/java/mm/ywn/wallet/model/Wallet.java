package mm.ywn.wallet.model;

import lombok.Getter;
import mm.ywn.common.util.Snowflake;
import mm.ywn.wallet.exception.InvalidTransactionTypeException;
import mm.ywn.wallet.exception.NegativeAmountException;

import java.math.BigDecimal;
import java.time.Instant;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

@Getter
public class Wallet {
    private WalletId id;
    private String name;
    private BigDecimal balance;

    private Instant addedOn;

    private List<BalanceLedger> balanceLedgerList = new ArrayList<>();

    public Wallet(String name, BigDecimal balance) {
        this.id = new WalletId(Snowflake.get().nextId());
        this.name = name;
        this.balance = balance;
        this.addedOn = Instant.now();
    }

    public Wallet(WalletId walletId, String name, BigDecimal balance) {
        this.id = walletId;
        this.name = name;
        this.balance = balance;
        this.addedOn = Instant.now();
    }

    private BalanceLedger deposit(Wallet sourceWallet, BigDecimal amount, String description) throws NegativeAmountException {
        if (amount.longValue() < 0) {
            throw new NegativeAmountException();
        }
        var before = this.balance;
        this.balance = this.balance.add(amount);
        return new BalanceLedger(new BalanceLedgerId(Snowflake.get().nextId()), this, sourceWallet, amount, before, this.balance, description, Instant.now());

    }

    private BalanceLedger withdraw(Wallet destinationWallet, BigDecimal amount, String description) throws NegativeAmountException {
        if (amount.longValue() < 0) {
            throw new NegativeAmountException();
        }
        var before = this.balance;
        this.balance = this.balance.subtract(amount);
        return new BalanceLedger(new BalanceLedgerId(Snowflake.get().nextId()), destinationWallet, this, amount, before, this.balance, description, Instant.now());

    }

    public void processTransaction(TransactionType transaction, Wallet sourceOrDestinationWallet, BigDecimal amount, String description) throws NegativeAmountException, InvalidTransactionTypeException {
        BalanceLedger balanceLedger;
        if (transaction == TransactionType.DEPOSIT) {
            balanceLedger = deposit(sourceOrDestinationWallet, amount, description);
        } else if (transaction == TransactionType.WITHDRAW) {
            balanceLedger = withdraw(sourceOrDestinationWallet, amount, description);
        } else {
            throw new InvalidTransactionTypeException("Invalid transaction type: " + transaction.name());
        }
        balanceLedgerList.add(balanceLedger);
    }


    public List<BalanceLedger> getBalanceLedgerList() {
        return Collections.unmodifiableList(this.balanceLedgerList);
    }
}
