package mm.ywn.wallet.model;

import java.io.Serial;
import java.io.Serializable;

public record WalletId (long id){}