package mm.ywn.wallet.model;

import lombok.Getter;

import java.math.BigDecimal;
import java.time.Instant;

@Getter
public class BalanceLedger {

    private BalanceLedgerId balanceLedgerId;

    private Wallet wallet;

    private Wallet from;

    private BigDecimal before;

    private BigDecimal after;

    private String description;

    private Instant transactionDate;

    private BigDecimal transactionAmount;

    private TransactionType transactionType;

    public BalanceLedger(BalanceLedgerId balanceLedgerId, Wallet from, Wallet wallet, BigDecimal transactionAmount, BigDecimal before, BigDecimal after, String description, Instant transactionDate) {
        this.balanceLedgerId = balanceLedgerId;
        this.wallet = wallet;
        this.before = before;
        this.after = after;
        this.description = description;
        this.transactionDate = transactionDate;
        this.transactionAmount = transactionAmount;
        this.from =from;
    }
}
