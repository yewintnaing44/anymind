package mm.ywn.wallet.repository;

import mm.ywn.common.paging.PagedRequest;
import mm.ywn.common.paging.PagedResult;
import mm.ywn.wallet.exception.WalletNotFoundException;
import mm.ywn.wallet.model.BalanceLedger;
import mm.ywn.wallet.model.Wallet;
import mm.ywn.wallet.model.WalletId;

import java.time.Instant;
import java.util.List;

public interface ReadOnlyWalletRepository {

    Wallet getWalletById(WalletId walletId) throws WalletNotFoundException;

    Wallet findById(WalletId walletId);

    List<Wallet> getAllWallets();

    PagedResult<BalanceLedger> getBalanceLedgerHistory(Instant to, Instant from, WalletId walletId, PagedRequest pagedRequest);
}
