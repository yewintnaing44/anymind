package mm.ywn.wallet.repository;

import mm.ywn.wallet.exception.WalletNotFoundException;
import mm.ywn.wallet.model.Wallet;
import mm.ywn.wallet.model.WalletId;

import java.math.BigDecimal;

public interface WalletRepository {

    long createWallet(Wallet wallet);

    void update(Wallet wallet) throws WalletNotFoundException;


}
