package mm.ywn.wallet.event;

import mm.ywn.wallet.model.TransactionType;

import java.math.BigDecimal;

public record TransactionCreatedEvent(String sourceWalletId, String destinationWalletId, BigDecimal amount,
                                      TransactionType type) {

}
