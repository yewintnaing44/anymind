package mm.ywn.wallet.exception;

import mm.ywn.common.exception.ApplicationException;
import mm.ywn.common.exception.ExceptionMessage;
import mm.ywn.common.misc.LanguageType;

import java.util.Optional;

public class NegativeAmountException extends ApplicationException {

    @Override
    public Optional<ExceptionMessage> asMessage() {
        var message = ExceptionMessage.of(this);
        message.addText(LanguageType.ENGLISH, "Amount cannot be negative.");

        return Optional.of(message);
    }
}
