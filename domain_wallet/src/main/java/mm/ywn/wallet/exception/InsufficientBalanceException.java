package mm.ywn.wallet.exception;

import mm.ywn.common.exception.ApplicationException;
import mm.ywn.common.exception.ExceptionMessage;
import mm.ywn.common.misc.LanguageType;

import java.util.Optional;

public class InsufficientBalanceException extends ApplicationException {

    private String message;

    public InsufficientBalanceException(String message) {
        this.message = message;
    }

    @Override
    public Optional<ExceptionMessage> asMessage() {
        var message = ExceptionMessage.of(this);
        message.addText(LanguageType.ENGLISH, this.message);

        return Optional.of(message);
    }
}
