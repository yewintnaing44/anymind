package mm.ywn.wallet.service;

import mm.ywn.wallet.exception.InvalidTransactionTypeException;
import mm.ywn.wallet.exception.NegativeAmountException;
import mm.ywn.wallet.exception.WalletNotFoundException;
import mm.ywn.wallet.model.TransactionType;
import mm.ywn.wallet.model.Wallet;
import mm.ywn.wallet.model.WalletId;
import mm.ywn.wallet.repository.ReadOnlyWalletRepository;
import mm.ywn.wallet.repository.WalletRepository;

import java.math.BigDecimal;

public class WalletServiceImpl implements WalletService {

    private final WalletRepository walletRepository;
    private final ReadOnlyWalletRepository readOnlyWalletRepository;


    public WalletServiceImpl(WalletRepository walletRepository, ReadOnlyWalletRepository readOnlyWalletRepository) {
        this.walletRepository = walletRepository;
        this.readOnlyWalletRepository = readOnlyWalletRepository;
    }

    @Override
    public void performTransaction(WalletId sourceWalletId, WalletId destinationWalletId, BigDecimal amount, String description, TransactionType transactionType) throws WalletNotFoundException, NegativeAmountException, InvalidTransactionTypeException {
        Wallet sourceWallet = this.readOnlyWalletRepository.findById(sourceWalletId);

        if (sourceWallet == null) {
            throw new WalletNotFoundException("Source wallet not found");
        }

        Wallet destinationWallet = this.readOnlyWalletRepository.findById(destinationWalletId);

        if (destinationWallet == null) {
            throw new WalletNotFoundException("Destination wallet not found");
        }

        sourceWallet.processTransaction(transactionType, destinationWallet, amount, description);


        this.walletRepository.update(sourceWallet);


    }

    @Override
    public long createWallet(WalletId walletId, String name, BigDecimal initialBalance) {

        return this.walletRepository.createWallet(new Wallet(walletId, name, initialBalance));
    }
}
