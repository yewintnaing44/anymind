package mm.ywn.wallet.service;

import mm.ywn.wallet.exception.InvalidTransactionTypeException;
import mm.ywn.wallet.exception.NegativeAmountException;
import mm.ywn.wallet.exception.WalletNotFoundException;
import mm.ywn.wallet.model.TransactionType;
import mm.ywn.wallet.model.WalletId;

import java.math.BigDecimal;

public interface WalletService {

    void performTransaction(WalletId sourceWalletId, WalletId destinationWalletId, BigDecimal amount, String description, TransactionType transactionType) throws WalletNotFoundException, NegativeAmountException, InvalidTransactionTypeException;

    long createWallet(WalletId walletId, String name, BigDecimal initialBalance);
}
