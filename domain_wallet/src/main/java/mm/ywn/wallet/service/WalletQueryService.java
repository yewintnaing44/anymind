package mm.ywn.wallet.service;

import mm.ywn.common.paging.PagedRequest;
import mm.ywn.common.paging.PagedResult;
import mm.ywn.wallet.exception.WalletNotFoundException;
import mm.ywn.wallet.model.BalanceLedger;
import mm.ywn.wallet.model.WalletId;

import java.time.Instant;

public interface WalletQueryService {

    public WalletDTO getWalletById(WalletId walletId) throws WalletNotFoundException;

    public PagedResult<BalanceLedger> getBalanceLedgerHistory(
            Instant to, Instant from, WalletId walletId, PagedRequest pagedRequest
    ) throws WalletNotFoundException;
}
