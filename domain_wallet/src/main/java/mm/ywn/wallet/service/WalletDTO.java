package mm.ywn.wallet.service;

import mm.ywn.wallet.model.WalletId;

import java.math.BigDecimal;

public record WalletDTO(WalletId walletId, String name, BigDecimal balance) {
}
