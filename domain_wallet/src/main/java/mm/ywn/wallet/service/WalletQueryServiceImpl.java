package mm.ywn.wallet.service;

import mm.ywn.common.paging.PagedRequest;
import mm.ywn.common.paging.PagedResult;
import mm.ywn.wallet.exception.WalletNotFoundException;
import mm.ywn.wallet.model.BalanceLedger;
import mm.ywn.wallet.model.WalletId;
import mm.ywn.wallet.repository.ReadOnlyWalletRepository;

import java.time.Instant;

public class WalletQueryServiceImpl implements WalletQueryService {

    private final ReadOnlyWalletRepository readOnlyWalletRepository;

    public WalletQueryServiceImpl(ReadOnlyWalletRepository readOnlyWalletRepository) {
        this.readOnlyWalletRepository = readOnlyWalletRepository;
    }

    @Override
    public WalletDTO getWalletById(WalletId walletId) throws WalletNotFoundException {

        var wallet = this.readOnlyWalletRepository.getWalletById(walletId);
        return new WalletDTO(wallet.getId(), wallet.getName(), wallet.getBalance());
    }

    @Override
    public PagedResult<BalanceLedger> getBalanceLedgerHistory(Instant to, Instant from, WalletId walletId, PagedRequest pagedRequest) throws WalletNotFoundException {
        return this.readOnlyWalletRepository.getBalanceLedgerHistory(to, from, walletId, pagedRequest);
    }
}
