package mm.ywn.common.config;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

@Configuration
@ComponentScan("mm.ywn.common")
public class ApplicationConfiguration {

}
