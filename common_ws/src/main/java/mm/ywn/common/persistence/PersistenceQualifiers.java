package mm.ywn.common.persistence;

public class PersistenceQualifiers {

    public static final String JPA = "jpa";

    public static final String AEROSPIKE = "aerospike";

    public static final String IGNITE = "ignite";

    public static final String REDIS = "redis";

    public static final String PROXIED = "proxied";

    public static final String IN_MEMORY = "memory";

}