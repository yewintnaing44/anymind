package mm.ywn.common.persistence.identifier;



import lombok.Getter;
import org.apache.commons.lang3.Validate;

import javax.persistence.AttributeConverter;
import javax.persistence.Converter;
import java.io.Serializable;
import java.util.regex.Pattern;

@Getter
public class Mobile implements Serializable {


    @Converter
    public static class JpaConverter implements AttributeConverter<Mobile, String> {

        @Override
        public String convertToDatabaseColumn(Mobile attribute) {

            return attribute == null ? null : attribute.value;

        }

        @Override
        public Mobile convertToEntityAttribute(String dbData) {

            return dbData == null ? null : new Mobile(dbData);

        }

    }


    public static final String FORMAT = "^(09|9)\\d{7,}$";

    private static final Pattern PATTERN = Pattern.compile(FORMAT);

    private String value;

    public Mobile(String value) {

        Validate.notNull(value, "Value is required.");

        if (!PATTERN.matcher(value).matches()) {

            throw new IllegalArgumentException("Value is in wrong format.");

        }

        this.value = value;

    }

}
