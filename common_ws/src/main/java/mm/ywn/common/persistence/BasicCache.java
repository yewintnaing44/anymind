package mm.ywn.common.persistence;

import java.util.List;

public interface BasicCache<ID, DATA, EXAMPLE> {

    void delete(ID id);

    List<DATA> find(EXAMPLE example);

    DATA get(ID id);

    void save(ID id, DATA data);

}
