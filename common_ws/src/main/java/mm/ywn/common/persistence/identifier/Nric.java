package mm.ywn.common.persistence.identifier;



import lombok.Getter;
import org.apache.commons.lang3.Validate;

import javax.persistence.AttributeConverter;
import javax.persistence.Converter;
import java.io.Serializable;

@Getter
public class Nric implements Serializable {


    @Converter
    public static class JpaConverter implements AttributeConverter<Nric, String> {

        @Override
        public String convertToDatabaseColumn(Nric attribute) {

            return attribute == null ? null : attribute.value;

        }

        @Override
        public Nric convertToEntityAttribute(String dbData) {

            return dbData == null ? null : new Nric(dbData);

        }

    }


    private String value;

    public Nric(String value) {

        Validate.notNull(value, "Value is required.");

        this.value = value;

    }

}
