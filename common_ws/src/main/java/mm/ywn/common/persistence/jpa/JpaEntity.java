package mm.ywn.common.persistence.jpa;

import lombok.EqualsAndHashCode;
import lombok.Setter;

import javax.persistence.*;
import java.io.Serializable;
import java.time.Instant;

@Setter
@MappedSuperclass
@EqualsAndHashCode
public abstract class JpaEntity implements Serializable {

    @Column(name = "created_date", updatable = false)
    @Convert(converter = JpaInstantConverter.class)
    protected Instant createdDate;

    @Column(name = "updated_date")
    @Convert(converter = JpaInstantConverter.class)
    protected Instant updatedDate;

    @Column(name = "version")
    @Version
    protected Integer version;

    @PrePersist
    public void prePersist() {

        this.createdDate = Instant.now();
        this.updatedDate = Instant.now();

    }

    @PreUpdate
    public void preUpdate() {

        this.updatedDate = Instant.now();

    }

}