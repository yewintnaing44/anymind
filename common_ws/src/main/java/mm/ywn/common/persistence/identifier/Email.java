package mm.ywn.common.persistence.identifier;


import lombok.Getter;
import org.apache.commons.lang3.Validate;

import javax.persistence.AttributeConverter;
import javax.persistence.Converter;
import java.io.Serializable;
import java.util.regex.Pattern;

@Getter
public class Email implements Serializable {



    @Converter
    public static class JpaConverter implements AttributeConverter<Email, String> {

        @Override
        public String convertToDatabaseColumn(Email attribute) {

            return attribute == null ? null : attribute.value;

        }

        @Override
        public Email convertToEntityAttribute(String dbData) {

            return dbData == null ? null : new Email(dbData);

        }

    }

    /**
     * 
     */
    private static final long serialVersionUID = 1L;

    public static final String FORMAT = "^[a-zA-Z0-9]+([\\.\\-_]{0,1}[a-zA-Z0-9]+)+@[a-zA-Z0-9]+([\\.\\-_]{0,1}[a-zA-Z0-9]+)+(\\.[a-zA-Z0-9]+)+$";

    private static final Pattern PATTERN = Pattern.compile(FORMAT);

    private String value;

    public Email(String value) {

        Validate.notNull(value, "Value is required.");

        if (!PATTERN.matcher(value).matches()) {

            throw new IllegalArgumentException("Value is in wrong format.");

        }

        this.value = value;

    }

}
