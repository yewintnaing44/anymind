package mm.ywn.common.persistence;

import java.util.List;
import java.util.Optional;

public interface BasicRepository<DOMAIN, ID> {

    public void delete(ID id);

    public Optional<DOMAIN> find(ID id);

    public DOMAIN get(ID id);

    public List<DOMAIN> list();

    public void save(DOMAIN domain);

}