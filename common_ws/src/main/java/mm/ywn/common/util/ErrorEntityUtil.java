package mm.ywn.common.util;

import java.util.Optional;

import javax.validation.ValidationException;

import mm.ywn.common.exception.ApplicationException;
import mm.ywn.common.exception.ErrorEntity;
import mm.ywn.common.exception.ExceptionMessage;
import mm.ywn.common.misc.LanguageType;
import mm.ywn.common.misc.Text;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.MethodArgumentNotValidException;


public class ErrorEntityUtil {

    private static final Logger LOG = LoggerFactory.getLogger(ErrorEntityUtil.class);

    public static ErrorEntity buildErrorEntity(Exception exception) {

        LOG.error("Exception occurred : [{}]", exception.getClass().getName());

        if (exception instanceof ApplicationException applicationException) {

            Optional<ExceptionMessage> optExceptionMessage = applicationException.asMessage();

            ErrorEntity errorEntity = ErrorEntity.of(applicationException.asCode());

            if (optExceptionMessage.isPresent()) {

                ExceptionMessage exceptionMessage = optExceptionMessage.get();

                exceptionMessage.getTexts().forEach(errorEntity::addMessage);

            } else {

                errorEntity
                        .addMessage(new Text(LanguageType.ENGLISH, "Problem occurred while processing your request."));

            }

            return errorEntity;

        } else if (exception instanceof ValidationException || exception instanceof MethodArgumentNotValidException) {

            ErrorEntity errorEntity = ErrorEntity.of("INPUT_EXCEPTION");

            errorEntity.addMessage(new Text(LanguageType.ENGLISH, "Problem in data inputs." + exception.getMessage()));

            return errorEntity;

        } else {

            ErrorEntity errorEntity = ErrorEntity.of("UNKNOWN_EXCEPTION");

            errorEntity.addMessage(new Text(LanguageType.ENGLISH, "Something went wrong. Please try again."));

            return errorEntity;

        }

    }

}
