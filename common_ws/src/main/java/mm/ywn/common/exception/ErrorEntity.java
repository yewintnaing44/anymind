package mm.ywn.common.exception;

import java.util.HashSet;
import java.util.Set;

import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;
import mm.ywn.common.misc.Text;

@Getter
@NoArgsConstructor(access = AccessLevel.PRIVATE)
public final class ErrorEntity {

    public static ErrorEntity of(String errorCode) {

        ErrorEntity entity = new ErrorEntity();

        entity.errorCode = errorCode;

        return entity;

    }

    private String errorCode;

    private final Set<Text> messages = new HashSet<Text>();

    public void addMessage(Text message) {

        this.messages.add(message);

    }

}
