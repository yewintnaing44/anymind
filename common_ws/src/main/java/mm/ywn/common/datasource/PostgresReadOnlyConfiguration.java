package mm.ywn.common.datasource;

import javax.persistence.EntityManagerFactory;
import javax.sql.DataSource;

import mm.ywn.common.datasource.setting.PostgresSetting;
import mm.ywn.common.persistence.ReadOnlyRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.FilterType;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.orm.jpa.vendor.HibernateJpaVendorAdapter;
import org.springframework.transaction.annotation.EnableTransactionManagement;

import com.zaxxer.hikari.HikariConfig;
import com.zaxxer.hikari.HikariDataSource;

import java.util.Properties;


@EnableTransactionManagement
@EnableJpaRepositories(
        basePackages = {"mm.ywn"},
        includeFilters = @ComponentScan.Filter(type = FilterType.ANNOTATION, classes = {ReadOnlyRepository.class}),
        entityManagerFactoryRef = "readOnlyEntityManagerFactory",
        transactionManagerRef = "readOnlyTransactionManager")
public class PostgresReadOnlyConfiguration {


    @Autowired
    @Qualifier("readOnlyPostgresSetting")
    private PostgresSetting postgresSetting;

    @Bean(name = "readOnlyDataSource", destroyMethod = "close")
    public DataSource dataSource() {

        HikariConfig config = new HikariConfig();

        config.setJdbcUrl(postgresSetting.url());
        config.setUsername(postgresSetting.username());
        config.setPassword(postgresSetting.password());
        config.setDriverClassName(postgresSetting.driver());

        config.addDataSourceProperty("cachePrepStmts", "true");
        config.addDataSourceProperty("prepStmtCacheSize", "250");
        config.addDataSourceProperty("prepStmtCacheSqlLimit", "2048");
        config.addDataSourceProperty("useServerPrepStmts", true);
        config.addDataSourceProperty("useLocalSessionState", true);
        config.addDataSourceProperty("rewriteBatchedStatements", true);
        config.addDataSourceProperty("cacheResultSetMetadata", true);
        config.addDataSourceProperty("cacheServerConfiguration", true);
        config.addDataSourceProperty("elideSetAutoCommits", true);
        config.addDataSourceProperty("maintainTimeStats", false);

        config.setMaximumPoolSize(postgresSetting.poolSize());

        return new HikariDataSource(config);

    }

    @Bean(name = "readOnlyEntityManagerFactory")
    public LocalContainerEntityManagerFactoryBean entityManagerFactory(DataSource readOnlyDataSource) {

        LocalContainerEntityManagerFactoryBean entityManagerFactoryBean = new LocalContainerEntityManagerFactoryBean();
        entityManagerFactoryBean.setDataSource(readOnlyDataSource);
        entityManagerFactoryBean.setJpaVendorAdapter(new HibernateJpaVendorAdapter());

        entityManagerFactoryBean.setPackagesToScan("mm.ywn.**");

        Properties jpaProperties = new Properties();

        jpaProperties.put("hibernate.dialect", postgresSetting.dialect()); // env.getRequiredProperty("jdbc.hibernate.dialect"));
        jpaProperties.put("hibernate.show_sql", postgresSetting.showSql()); // env.getRequiredProperty("jdbc.hibernate.show_sql"));
        jpaProperties.put("hibernate.format_sql", postgresSetting.formatSql()); // env.getRequiredProperty("jdbc.hibernate.format_sql"));

        jpaProperties.put("hibernate.default_schema", postgresSetting.defaultSchema()); // env.getRequiredProperty("jdbc.hibernate.default_schema"));


        entityManagerFactoryBean.setJpaProperties(jpaProperties);

        return entityManagerFactoryBean;

    }

    @Bean(name = "readOnlyTransactionManager")
    public JpaTransactionManager transactionManager(
            @Qualifier("readOnlyEntityManagerFactory") EntityManagerFactory entityManagerFactory) {

        JpaTransactionManager transactionManager = new JpaTransactionManager();

        transactionManager.setEntityManagerFactory(entityManagerFactory);

        return transactionManager;

    }

//    @Bean(name = "readOnlyJdbcTemplate")
//    public JdbcTemplate readOnlyJdbcTemplate(@Qualifier("readOnlyDataSource") DataSource readOnlyDataSource) {
//
//        return new JdbcTemplate(readOnlyDataSource);
//
//    }

}
