package mm.ywn.common.datasource.setting;

public interface PostgresSetting {
    String url();

    String username();

    String password();

    String driver();

    int poolSize();

    String dialect();

    boolean showSql();

    boolean formatSql();

    String defaultSchema();

}
