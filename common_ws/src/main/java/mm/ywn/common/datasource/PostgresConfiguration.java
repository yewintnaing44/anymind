package mm.ywn.common.datasource;

import java.util.Properties;


import mm.ywn.common.datasource.setting.PostgresSetting;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.orm.jpa.vendor.HibernateJpaVendorAdapter;
import org.springframework.transaction.annotation.EnableTransactionManagement;

import com.zaxxer.hikari.HikariConfig;
import com.zaxxer.hikari.HikariDataSource;

import javax.persistence.EntityManagerFactory;
import javax.sql.DataSource;


@Configuration
@EnableTransactionManagement
@EnableJpaRepositories(basePackages = { "mm.ywn" }, considerNestedRepositories = true)
public class PostgresConfiguration {

    @Autowired
    @Qualifier("defaultPostgresSettings")
    private PostgresSetting postgresSetting;


    @Bean(destroyMethod = "close")
    public DataSource dataSource() {

        HikariConfig config = new HikariConfig();
        config.setJdbcUrl(postgresSetting.url());
        config.setUsername(postgresSetting.username());
        config.setPassword(postgresSetting.password());
        config.setDriverClassName(postgresSetting.driver());

        config.addDataSourceProperty("cachePrepStmts", "true");
        config.addDataSourceProperty("prepStmtCacheSize", "250");
        config.addDataSourceProperty("prepStmtCacheSqlLimit", "2048");
        config.addDataSourceProperty("useServerPrepStmts", true);
        config.addDataSourceProperty("useLocalSessionState", true);
        config.addDataSourceProperty("rewriteBatchedStatements", true);
        config.addDataSourceProperty("cacheResultSetMetadata", true);
        config.addDataSourceProperty("cacheServerConfiguration", true);
        config.addDataSourceProperty("elideSetAutoCommits", true);
        config.addDataSourceProperty("maintainTimeStats", false);

        config.setMaximumPoolSize(postgresSetting.poolSize());


        return new HikariDataSource(config);

    }

    @Bean
    @Primary
    public LocalContainerEntityManagerFactoryBean entityManagerFactory() {

        LocalContainerEntityManagerFactoryBean entityManagerFactoryBean = new LocalContainerEntityManagerFactoryBean();
        entityManagerFactoryBean.setDataSource(this.dataSource());
        entityManagerFactoryBean.setJpaVendorAdapter(new HibernateJpaVendorAdapter());

        entityManagerFactoryBean.setPackagesToScan("mm.ywn.**");

        Properties jpaProperties = new Properties();

        jpaProperties.put("hibernate.dialect", postgresSetting.dialect()); // env.getRequiredProperty("jdbc.hibernate.dialect"));
        jpaProperties.put("hibernate.show_sql", postgresSetting.showSql()); // env.getRequiredProperty("jdbc.hibernate.show_sql"));
        jpaProperties.put("hibernate.format_sql", postgresSetting.formatSql()); // env.getRequiredProperty("jdbc.hibernate.format_sql"));

        jpaProperties.put("hibernate.default_schema", postgresSetting.defaultSchema()); // env.getRequiredProperty("jdbc.hibernate.default_schema"));


        entityManagerFactoryBean.setJpaProperties(jpaProperties);

        return entityManagerFactoryBean;

    }

    @Bean
    @Primary
    public JpaTransactionManager transactionManager(EntityManagerFactory entityManagerFactory) {

        JpaTransactionManager transactionManager = new JpaTransactionManager();

        transactionManager.setEntityManagerFactory(entityManagerFactory);

        return transactionManager;

    }
}

