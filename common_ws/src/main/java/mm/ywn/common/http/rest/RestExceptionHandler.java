package mm.ywn.common.http.rest;

import mm.ywn.common.exception.ApplicationException;
import mm.ywn.common.exception.ErrorEntity;
import mm.ywn.common.util.ErrorEntityUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestControllerAdvice;

@RestControllerAdvice
public class RestExceptionHandler {

    private final static Logger LOG = LoggerFactory.getLogger(RestExceptionHandler.class);

    private ErrorEntity errorResponse(Exception exception) {

        return ErrorEntityUtil.buildErrorEntity(exception);

    }

    @ExceptionHandler(value = {ApplicationException.class})
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    @ResponseBody
    public ErrorEntity processApplicationException(ApplicationException ex) {

        LOG.error("ERROR : ", ex);

        return this.errorResponse(ex);

    }

    @ExceptionHandler(value = {Exception.class})
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    @ResponseBody
    public ErrorEntity processException(Exception ex) {

        LOG.error("ERROR : ", ex);

        return this.errorResponse(ex);

    }

    @ExceptionHandler(value = {RuntimeException.class})
    @ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
    @ResponseBody
    public ErrorEntity processRuntimeException(RuntimeException ex) {

        LOG.error("ERROR : ", ex);

        return this.errorResponse(ex);

    }

}