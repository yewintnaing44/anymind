package mm.ywn.user.exception;

import mm.ywn.common.exception.ApplicationException;
import mm.ywn.common.exception.ExceptionMessage;
import mm.ywn.common.misc.LanguageType;

import java.util.Optional;

public class UserNotFoundException extends ApplicationException {

    @Override
    public Optional<ExceptionMessage> asMessage() {
        var message = ExceptionMessage.of(this);
        message.addText(LanguageType.ENGLISH, "User not found.");

        return Optional.of(message);
    }
}
