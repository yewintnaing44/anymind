package mm.ywn.user.model;

import lombok.Getter;
import mm.ywn.common.util.Snowflake;

@Getter
public class User {

    private final UserId userId;

    private Email email;

    private String firstName;

    private String lastName;

    private Nrc nrc;

    private Mobile mobile;

    private Address address;


    public User(String firstName, String lastName, Nrc nrc, Mobile mobile, Email email) {
        this.userId = new UserId(Snowflake.get().nextId());
        this.email = email;
        this.firstName = firstName;
        this.lastName = lastName;
        this.nrc = nrc;
        this.mobile = mobile;
    }

    public User(long userId, String firstName, String lastName, Nrc nrc, Mobile mobile, Email email) {
        this.userId = new UserId(userId);
        this.email = email;
        this.firstName = firstName;
        this.lastName = lastName;
        this.nrc = nrc;
        this.mobile = mobile;
    }

    public void updateUserInfo(Email email, String firstName, String lastName, Nrc nrc, Mobile mobile) {
        this.email = email;
        this.firstName = firstName;
        this.lastName = lastName;
        this.nrc = nrc;
        this.mobile = mobile;
    }


    public void setAddress(String street, String city, String state, String zipCode) {
        this.address = new Address(street, city, state, zipCode);
    }

    public void updateAddress(String street, String city, String state, String zipCode) {
        this.address = new Address(street, city, state, zipCode);
    }
}
