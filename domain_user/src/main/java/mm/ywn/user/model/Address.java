package mm.ywn.user.model;

public record Address(String street, String city, String state, String zipCode) {
}