package mm.ywn.user.event;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import mm.ywn.common.converter.InstantToLong;
import mm.ywn.common.converter.LongToInstant;

import java.time.Instant;

public record  UserCreatedEvent(
        String id,
        String firstName,
        String lastName,
        String email,
        String nrc,
        String mobile,
        String street,
        String city,
        String state,
        String zipCode,
       @JsonSerialize(using = InstantToLong.class) @JsonDeserialize(using = LongToInstant.class) Instant createdAt) {
}