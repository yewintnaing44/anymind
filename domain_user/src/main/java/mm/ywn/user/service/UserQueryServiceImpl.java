package mm.ywn.user.service;

import mm.ywn.common.exception.ApplicationException;
import mm.ywn.user.exception.UserNotFoundException;
import mm.ywn.user.model.UserId;
import mm.ywn.user.repository.ReadOnlyUserRepository;

import java.util.List;

public class UserQueryServiceImpl implements UserQueryService {

    private final ReadOnlyUserRepository readOnlyUserRepository;

    public UserQueryServiceImpl(ReadOnlyUserRepository readOnlyUserRepository) {
        this.readOnlyUserRepository = readOnlyUserRepository;
    }

    @Override
    public UserDTO getUser(UserId userId) throws ApplicationException {
        var user = this.readOnlyUserRepository.findById(userId);

        if (user == null) {
            throw new UserNotFoundException();
        }

        return new UserDTO(user.getUserId(), user.getFirstName(), user.getLastName(), user.getNrc(), user.getMobile(), user.getEmail(), user.getAddress() == null ? null : new UserDTO.Address(user.getAddress().street(), user.getAddress().city(), user.getAddress().state(), user.getAddress().zipCode()));
    }

    @Override
    public List<UserDTO> findAll() {
        return this.readOnlyUserRepository.findAll().stream().map(user -> new UserDTO(user.getUserId(), user.getFirstName(), user.getLastName(), user.getNrc(), user.getMobile(), user.getEmail(), user.getAddress() == null ? null : new UserDTO.Address(user.getAddress().street(), user.getAddress().city(), user.getAddress().state(), user.getAddress().zipCode()))).toList();
    }
}
