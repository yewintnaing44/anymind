package mm.ywn.user.service;

import mm.ywn.common.exception.ApplicationException;
import mm.ywn.user.model.*;

import java.util.List;

public interface UserQueryService {
    public UserDTO getUser(UserId userId) throws ApplicationException;

    public List<UserDTO> findAll();


}
