package mm.ywn.user.service;

import mm.ywn.common.exception.ApplicationException;
import mm.ywn.user.exception.UserNotFoundException;
import mm.ywn.user.model.User;
import mm.ywn.user.model.UserId;
import mm.ywn.user.repository.ReadOnlyUserRepository;
import mm.ywn.user.repository.UserRepository;

public class UserServiceImpl implements UserService {

    private final UserRepository userRepository;

    private final ReadOnlyUserRepository readOnlyUserRepository;

    public UserServiceImpl(UserRepository userRepository, ReadOnlyUserRepository readOnlyUserRepository) {
        this.userRepository = userRepository;
        this.readOnlyUserRepository = readOnlyUserRepository;
    }

    @Override
    public long save(UserDTO input) {

        var user = new User(
                input.firstName(),
                input.lastName(),
                input.nrc(),
                input.mobile(),
                input.email()
        );

        if (input.address() != null) {
            user.setAddress(
                    input.address().street(),
                    input.address().city(),
                    input.address().state(),
                    input.address().zipCode()
            );
        }

        this.userRepository.save(user);

        return user.getUserId().id();
    }

    @Override
    public void update(UserId userId, UserDTO input) throws ApplicationException {

        var user = this.readOnlyUserRepository.findById(userId);

        if (user == null) {
            throw new UserNotFoundException();
        }

        user.updateUserInfo(
                input.email(),
                input.firstName(),
                input.lastName(),
                input.nrc(),
                input.mobile()
        );

        if (input.address() != null) {
            user.updateAddress(
                    input.address().street(),
                    input.address().city(),
                    input.address().state(),
                    input.address().zipCode()
            );
        }

        this.userRepository.save(user);

    }
}
