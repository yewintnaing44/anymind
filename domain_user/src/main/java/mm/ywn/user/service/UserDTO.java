package mm.ywn.user.service;

import mm.ywn.user.model.Email;
import mm.ywn.user.model.Mobile;
import mm.ywn.user.model.Nrc;
import mm.ywn.user.model.UserId;

public record UserDTO(UserId id,
                      String firstName,
                      String lastName,
                      Nrc nrc,
                      Mobile mobile,
                      Email email,
                      UserDTO.Address address) {
    public record Address(String street, String city, String state, String zipCode) {
    }
}
