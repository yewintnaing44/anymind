package mm.ywn.user.service;

import mm.ywn.common.exception.ApplicationException;
import mm.ywn.user.model.*;

public interface UserService {
    public long save(UserDTO input);

    public void update(UserId userId, UserDTO input) throws ApplicationException;


}
