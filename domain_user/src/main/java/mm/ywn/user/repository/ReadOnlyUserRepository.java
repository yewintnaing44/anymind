package mm.ywn.user.repository;

import mm.ywn.user.model.User;
import mm.ywn.user.model.UserId;

import java.util.List;

public interface ReadOnlyUserRepository {
    User findById(UserId id);
    List<User> findAll();
}
