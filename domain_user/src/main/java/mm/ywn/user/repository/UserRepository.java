package mm.ywn.user.repository;

import mm.ywn.user.model.User;

public interface UserRepository {
    void save(User user);

}
