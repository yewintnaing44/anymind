package mm.ywn.application.command.implementation;

import mm.ywn.application.command.RegisterUserCommand;
import mm.ywn.user.service.UserDTO;
import mm.ywn.user.service.UserService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

@Service
public class RegisterUserCommandImpl implements RegisterUserCommand {

    private final UserService userService;

    private final static Logger LOG = LoggerFactory.getLogger(RegisterUserCommandImpl.class);


    public RegisterUserCommandImpl(UserService userService) {
        this.userService = userService;
    }

    @Override
    public Output execute(Input input) {

        LOG.info("Input: firstName: {}, lastName: {}, mobile: {}, email: {}, nrc: {}", input.firstName(), input.lastName(), input.mobile(), input.email(), input.nrc());


        var userId = this.userService.save(new UserDTO(
                null,
                input.firstName(),
                input.lastName(),
                input.nrc(),
                input.mobile(),
                input.email(),
                input.address() != null ? new UserDTO.Address(
                        input.address().street(),
                        input.address().city(),
                        input.address().state(),
                        input.address().zipCode()
                ) : null));


        return new Output(userId);
    }
}
