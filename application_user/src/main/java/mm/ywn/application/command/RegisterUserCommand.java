package mm.ywn.application.command;

import mm.ywn.user.model.Email;
import mm.ywn.user.model.Mobile;
import mm.ywn.user.model.Nrc;
import mm.ywn.user.model.UserId;

public interface RegisterUserCommand {

    public Output execute(Input input);

    public record Input(
            String firstName,
            String lastName,
            Nrc nrc,
            Mobile mobile,
            Email email,
            Input.Address address) {
        public record Address(String street, String city, String state, String zipCode) {
        }
    }

    public record Output(long userId) {
    }
}
