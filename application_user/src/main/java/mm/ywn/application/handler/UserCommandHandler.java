package mm.ywn.application.handler;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import mm.ywn.application.command.RegisterUserCommand;
import mm.ywn.user.event.UserCreatedEvent;
import org.apache.kafka.common.protocol.Message;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.stereotype.Component;

import java.time.Instant;

@Component
public class UserCommandHandler {

    private final RegisterUserCommand registerUserCommand;

    private KafkaTemplate<String, String> kafkaTemplate;

    @Autowired
    public UserCommandHandler(RegisterUserCommand registerUserCommand, KafkaTemplate<String, String> kafkaTemplate) {
        this.registerUserCommand = registerUserCommand;
        this.kafkaTemplate = kafkaTemplate;
    }


    public long handleRegisterCommand(RegisterUserCommand.Input command) {
        var registerUserOutput = this.registerUserCommand.execute(command);

        var userCreatedEvent = new UserCreatedEvent(
                String.valueOf(registerUserOutput.userId()),
                command.firstName(),
                command.lastName(),
                command.nrc().id(),
                command.mobile().id(),
                command.email().id(),
                command.address() != null ? command.address().street() : null,
                command.address() != null ? command.address().city() : null,
                command.address() != null ? command.address().state() : null,
                command.address() != null ? command.address().zipCode() : null,
                Instant.now()
        );

        ObjectMapper objectMapper = new ObjectMapper();
        String json;
        try {
            json = objectMapper.writeValueAsString(userCreatedEvent);
        } catch (JsonProcessingException e) {
            throw new RuntimeException(e);
        }

        kafkaTemplate.send("user-created-topic", json);

        return registerUserOutput.userId();
    }


}
